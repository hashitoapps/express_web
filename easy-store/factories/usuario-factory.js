angular
  .module('easy-store')
  .factory('UsuarioFactory', ['$http', 'AppConst', function($http, AppConst) {
    var urlBase     = AppConst.Url;
    var dataFactory = {};

    dataFactory.InfoUserFB = function(fbToken) {
      //console.log(fbToken);
      return $http.post([
        urlBase,
        'usuarios',
        'facebook'
      ].join('/'), {
        access_token: fbToken
      });
    };

    dataFactory.Guardar = function(Id, usr) {
      return $http.put([urlBase, 'usuarios', Id].join('/'), usr);
    };

    dataFactory.GuardarAvatar = function(usr, imagenBase64) {
      /*var options = {
        fileKey: 'file',
        fileName: 'avatar.png',
        httpMethod: 'POST',
        mimeType: 'image/png'/*,
        params: '',
        chunkedMode: '',
        headers: '',*/
      /*};

      return $cordovaFileTransfer
        .upload([urlBase, 'usuarios', usr.Correo, 'avatar'].join('/'), filePath, options);*/

      return $http.post([urlBase, 'usuarios', usr.Correo, 'avatar'].join('/'), {
        Avatar: imagenBase64
      });
    };

    dataFactory.CargarUsuario = function(Id) {
      return $http.get([urlBase, 'usuarios', Id].join('/'));
    };

    dataFactory.UrlAvatar = function(user) {
      return [urlBase.replace('/api/v00',''), 'avatars', user.Correo + '.png'].join('/');
    };

    dataFactory.CargarPaises = function(){
      return $http.get([urlBase, 'paises'].join('/'));  
    }

    dataFactory.ObtenerDes = function(obj) {
      return $http.post([urlBase, 'usuarios','obtenerdes'].join('/'), obj);
    };

    dataFactory.ObtenerUsuarioPorEmail = function(obj) {
      return $http.post([urlBase, 'usuarios','obtenerporemail'].join('/'), obj);
    };

    dataFactory.RecuperarContra = function(obj) {
      return $http.post([urlBase, 'usuarios','recuperarcontra'].join('/'), obj);
    };

    return dataFactory;
  }]);
