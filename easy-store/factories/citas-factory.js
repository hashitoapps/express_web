angular
  .module('easy-store')
  .factory('CitasFactory', ['$http', 'AppConst', function($http, AppConst) {
    var urlBase     = AppConst.UrlCitas;
    var dataFactory = {};

    dataFactory.GuardarUsuarioEnCitas = function(usr) {
      return $http.post([urlBase, 'guardarUsuario'].join('/'),usr);
    };   

    return dataFactory;
  }]);
