angular
  .module('easy-store')
  .factory('PublicacionFactory', ['$http', 'AppConst', 'Upload', function($http, AppConst, Upload) {
    var urlBase     = AppConst.Url;
    var dataFactory = {};

    dataFactory.LoadLastPublications = function() {
      var url = [
        'productos',
        [
          'page=1',
          'pageSize=4',
          'orderBy=createdAt',
          'sortOrder=DESC',
        ].join('&')
      ].join('?');

      return $http.get([urlBase, url].join('/'));
    }

    dataFactory.LoadMostPopulars = function() {
      var url = [
        'productos',
        [
          'page=1',
          'pageSize=6',
          'orderBy=MeGusta',
          'sortOrder=DESC',
        ].join('&')
      ].join('?');

      return $http.get([urlBase, url].join('/'));
    }

    dataFactory.LoadMostViewed = function() {
      var url = [
        'productos',
        [
          'page=1',
          'pageSize=6',
          'orderBy=Visitas',
          'sortOrder=DESC',
        ].join('&')
      ].join('?');

      return $http.get([urlBase, url].join('/'));
    };

    dataFactory.LoadPublicationList = function(
      page,
      pageSize,
      orderBy,
      sortOrder,
      filter) {
      console.log("este es filter:"+filter);
      filter = filter.replace('IdSubCategoria=0','IdSubCategoria=');
      filter = filter.replace('IdVariedad=0','IdVariedad=');
      var url = [
        'productos/filter',
        [
          ['page', page].join('='),
          ['pageSize', pageSize].join('='),
          ['orderBy', orderBy].join('='),
          ['sortOrder', sortOrder].join('='),
          filter
        ].join('&')
      ].join('?');

      return $http.get([urlBase, url].join('/'));
    };

    dataFactory.LoadSimilarPublications = function(Id) {

      return $http.get([urlBase, 'productos', Id, 'similares'].join('/'));
    };

    dataFactory.LoadPublication = function(Id, noVisitar) {
      var urlVisitar   = [urlBase, 'productos', Id, 'visitar'].join('/');
      var urlNoVisitar = [urlBase, 'productos', Id].join('/');
      var urlPublicacion = !!noVisitar ? urlNoVisitar : urlVisitar;

      return $http.get(urlPublicacion);
    };

    dataFactory.SendContactMail = function(Id, contactInfo) {
      return $http.post([
        urlBase,
        'productos',
        Id,
        'correo-contacto'
      ].join('/'), contactInfo);
    };

    dataFactory.SendContactenosMail = function(contactInfo) {
      return $http.post([
        urlBase,
        'productos',
        'correo-contactenos'
      ].join('/'), contactInfo);
    };

    dataFactory.SendCommentMail = function(Id, commentInfo) {
      return $http.post([
        urlBase,
        'comentarios',
        Id,
        'correo-comentario'
      ].join('/'), commentInfo);
    };

    dataFactory.SendApprovePublication = function(Id, ApproveInfo) {
      return $http.post([
        urlBase,
        'productos',
        Id,
        'correo_aprobar'
      ].join('/'), ApproveInfo);
    };

    dataFactory.SendResultApproveMail = function(ResultApproveInfo) {
      return $http.post([
        urlBase,
        'productos',
        'correo_respuesta'
      ].join('/'), ResultApproveInfo);
    };

    dataFactory.LoadPublicationsByUserId = function(UserId) {
      return $http.get([
        urlBase,
        'usuarios',
        UserId,
        'productos'
      ].join('/'));
    };

    dataFactory.DeletePublicacion = function(publicacion) {
      return $http.delete([
        urlBase,
        'productos',
        publicacion.Id
      ].join('/'));
    };

    dataFactory.DeleteImagenPublicacion = function(imagen) {
      return $http.delete([
        urlBase,
        'imagenes',
        imagen.Id
      ].join('/'));
    };

    dataFactory.GuardarPublicacion = function(publicacion) {
      console.log("publicacion----------------"+publicacion);
      return $http.post([urlBase, 'productos'].join('/'), publicacion);
    };

    dataFactory.GuardarImagen = function(publicacion, file) {
      //console.log("publicacion.Id:"+publicacion.data.Id);
      return Upload.upload({
        url: [
          urlBase,
          'productos',
          publicacion.data.Id,
          'imagen'
        ].join('/'),
        data: {file: file, 'publicacion': publicacion}
      });
    };

    dataFactory.ActualizarPublicacion = function(Id, publicacion) {
      console.log("publicacion ***", publicacion);
      return $http.put([urlBase, 'productos' ,Id].join('/'), publicacion);
    };

    dataFactory.GuardarComentario = function(comentario) {
      return $http.post([urlBase, 'comentarios'].join('/'), comentario);
    };

    dataFactory.GuardarProductoMunicipio = function(param) {
      return $http.post([urlBase,'productomunicipio'].join('/'), param);
    }

    dataFactory.EliminarProductoMunicipio = function(param) {
      return $http.delete([urlBase,'productomunicipio',param,'deleteall'].join('/'));
    }

    dataFactory.ConsultarProductoMunicipioPorProducto = function(param) {
      return $http.get([urlBase,'productomunicipio',param,'getbyproduct'].join('/'));
    }

    

    return dataFactory;
  }]);
