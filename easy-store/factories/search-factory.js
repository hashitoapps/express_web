angular
  .module('easy-store')
  .factory('SearchFactory', ['$http', 'AppConst', function($http, AppConst) {
    var urlBase     = AppConst.Url;
    var dataFactory = {};

    dataFactory.LoadCategories = function() {
      return $http.get([urlBase, 'subcategorias'].join('/'));
    };

    dataFactory.LoadDepartamentos = function(idpais) {
      return $http.get([urlBase, 'departamentos',idpais].join('/'));
    };

    dataFactory.LoadImagenesBanner = function() {
      return $http.get([urlBase, 'imagenes'].join('/'));
    };

     dataFactory.LoadCategoriesId = function(Id) {
      return $http.get([ urlBase,'subcategorias', Id
      ].join('/'));
    };

     dataFactory.LoadCategoriesIdFiltro = function(Id) {
      return $http.get([ urlBase,'subcategorias', Id
      ].join('/'));
    };

    /*dataFactory.LoadComentarios = function() {
      return $http.get([urlBase, 'comentarios'].join('/'));
    };*/

    dataFactory.LoadComentarios = function(Id) {
      return $http.get([urlBase, 'comentarios', Id, 'todos'].join('/'));
    };

    return dataFactory;
  }]);
