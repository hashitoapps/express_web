angular
  .module('easy-store')
  .factory('SuscribeFactory', ['$http', 'AppConst', function($http, AppConst) {
    var urlBase     = AppConst.Url;
    var dataFactory = {};

    dataFactory.Suscribir = function(suscribeForm) {
      return $http.post([
        urlBase,
        'suscribir'
      ].join('/'), suscribeForm);
    };

    return dataFactory;
  }]);
