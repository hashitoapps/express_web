angular
  .module('easy-store')
  .controller('ImagenesController', [
    '$scope',
    '$routeParams',
    '$location',
    '$anchorScroll',
    '$sce',
    'SearchFactory',
    function(
      $scope,
      $routeParams,
      $location,
      $anchorScroll,
      $sce,
      SearchFactory) {
      var vm = this;

      vm.rutas = [];

function init() {
  console.log("Entro aqui para guaradar imagenes");
      vm.CargarImagenes = function() {
        SearchFactory
          .LoadImagenesBanner()
          .then(function(result) {
            vm.rutas = result.data.rows;
          })
          .catch(function(error) {
            console.log(error);
            //alert('Se presento un error');
          });

        var rutas = [].filter.call(vm.rutas, function(item) {
          return item.ruta == vm.RutaGrande;
        })[0];
      };
    };

      return vm;
    }]);
