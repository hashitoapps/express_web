angular
  .module('easy-store')
  .controller('PublicacionesUsuarioController', [
    '$scope',
    '$location',
    '$anchorScroll',
    '$auth',
    '$http',
    'PublicacionFactory',
    'UsuarioFactory',
    '$routeParams',
    function($scope, $location, $anchorScroll, $auth, $http, PublicacionFactory,UsuarioFactory,$routeParams) {
      var vm = this;

      $location.hash('top');
      $anchorScroll();
       console.log('Entro por qui por el usuario');



      //criferlo
      function loadUser() {
        vm.user          = {};
        vm.files         = [];
        vm.usuario       = {};
        vm.userLogged    = $auth.isAuthenticated();
        vm.contactForm   = {};
        vm.cambiarAvatar = false

        if (vm.userLogged && !$auth.getPayload()) {

          UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {
              console.log("selogueafb");
              vm.user       = result.data;
              console.log("xxxuserid:"+vm.user.Id);
              vm.userLogged = $auth.isAuthenticated();

              vm.user.Telefono         = vm.user.Telefono * 1;
              vm.user.Identificacion   = vm.user.Identificacion * 1;
              vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
              //criferlo
              init();
            })
            .catch(function(error) {
              console.log('catch');
              console.log(error);

              vm.userLogged = false;
            });
        } else if(vm.userLogged && !!$auth.getPayload()) {
          vm.user = $auth.getPayload().sub;

          vm.user.Telefono         = vm.user.Telefono * 1;
          vm.user.Identificacion   = vm.user.Identificacion * 1;
          vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
          //criferlo
          init();
        }

      };

      function init() {

        vm.user = {};
        vm.user.Id = $routeParams.Id;
        vm.user.Nombre = $routeParams.Nombre;
        console.log("hola:"+$routeParams.Id);
        vm.publicaciones = [];
        //console.log(vm.user.Id);
        PublicacionFactory
          .LoadPublicationsByUserId(vm.user.Id)
          .then(function(result) {
            vm.publicaciones = result.data.rows;
            console.log("publicacionewwwwwxxxx:"+vm.publicaciones);
          })
          .catch(function(error) {
            console.log(error);
          })
      }

      //criferlo
      //loadUser();

      init();

      return vm;
    }]);


    /*$( document ).ready(function() {
      setTimeout(function(){
           vm.SendApprovePublication();
       }, 2000);
    });*/
