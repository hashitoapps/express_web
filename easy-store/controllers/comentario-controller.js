angular
  .module('easy-store')
  .controller('ComentarioController', [
    '$scope',
    '$routeParams',
    '$location',
    '$anchorScroll',
    '$sce',
    'SearchFactory',
    function(
      $scope,
      $routeParams,
      $location,
      $anchorScroll,
      $sce,
      SearchFactory) {
      var vm = this;

      vm.rutas = [];

    function init() {
    };

    vm.CargarComentarios = function(id) {
      console.log("Entro aquiii",id );
      SearchFactory
        .LoadComentarios(id)
        .then(function(result) {
          vm.rutas = result.data.rows;
          console.log("Rutas",vm.rutas);
        })
        .catch(function(error) {
          console.log(error);
        });

      var rutas = [].filter.call(vm.rutas, function(item) {
        return item.ruta == vm.Comentario;
      })[0];
    };

      return vm;
    }]);
