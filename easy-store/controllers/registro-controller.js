angular
  .module('easy-store')
  .controller('RegistroController', [
    '$scope',
    'UsuarioFactory',
    'CitasFactory',
    '$auth',
    '$http',
    '$window',
    function($scope, UsuarioFactory,CitasFactory, $auth, $http, $window) {
      var vm = this;
      vm.paises = [];
      vm.paisSeleccionado={};
      vm.paisesX = [];
      vm.Clave2 = null;
      vm.Usuario = {};


      function init(){
        console.log("si estoy aqui");
        vm.terminos=true;


        //cargar paises
        vm.LoadPaises();

      }

      vm.LoadPaises = function(){

        UsuarioFactory.CargarPaises()
        .then(function(result){

          vm.paises = result.data.rows;

          vm.paises.forEach(function(ele){
            vm.paisesX.push({id: ele.Id,name:ele.Nombre});
          })
          vm.paisSeleccionado = vm.paisesX[4];


          //console.log("paises"+paises);
        });
      }

      vm.SignUp = function () {

        if(vm.Clave2 != vm.usuario.Clave){
          alert('Las contraseñas no coinciden');
        }else{
          if(vm.paisSeleccionado==undefined){
            alert('Por favor elija un país');
          }
          else{
             console.log(vm.usuario);
              vm.usuario.ISFACEBOOK = false;
              vm.usuario.PaiId = vm.paisSeleccionado.id;

              //citas
              $auth
                .signup(vm.usuario)
                .then(function(result) {

                  vm.usuario.Id= $auth.getPayload().sub.Id;

                  //Guarda el usuario en el sistema de citas
                  CitasFactory
                    .GuardarUsuarioEnCitas(vm.usuario)
                    .then(function(result){

                      //Guarda el id del sistema de citas
                      vm.usuario.UsuarioCitasId=result.data.id;

                      UsuarioFactory
                      .Guardar(vm.usuario.Id,vm.usuario)
                      .then(function(res){


                          //Luego de guardar loguea
                          $auth.login(vm.usuario)
                           .then(function(res){
                               //Cierra los avisos y entra al sistema
                               vm.showAlertRegistro();
                               setTimeout(function(){ vm.closeAlertRegistro(); }, 2000);
                           })
                           .catch(function(err){
                               console.log("ocurrió error logueando el usuario:"+err);
                           })


                      })
                      .catch(function(error){
                        console.log(error);
                      })

                    })
                    .catch(function(err){
                      console.log("ocurrió error guardando en sistema de citas"+err)
                    })


                })
                .catch(function(error) {
                  console.log(error);
                  console.log(error.data.name);
                  if (error.data.name=='SequelizeUniqueConstraintError') {
                    alert('El correo que esta utilizando ya esta registrado. De clic en Ingreso.');
                  } else {
                    alert('Se ha presentado un error al registrar su usuario.');
                  }
                });
          }
        }



      };

      vm.FBLogin = function() {
        $auth
          .authenticate('facebook')
          .then(function(result) {
            console.log("logueo por facebook");
            //console.log(result.data.access_token);

            UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {
              vm.user       = result.data;

              //Guardar el proveedor
              vm.user.Proveedor = vm.usuario.Proveedor;
              console.log(vm.user.Proveedor);

              UsuarioFactory
                .Guardar(vm.user.Id,vm.user)
                .then(function(res){

                    //Guardar en Citas Medidas
                     //Guarda el usuario en el sistema de citas
                    CitasFactory
                      .GuardarUsuarioEnCitas(vm.user)
                      .then(function(result){

                        //Guarda el id del sistema de citas
                        vm.user.UsuarioCitasId=result.data.id;

                        UsuarioFactory
                        .Guardar(vm.user.Id,vm.user)
                        .then(function(res){
                          init();
                          vm.showAlertRegistro();
                          setTimeout(function(){ vm.closeAlertRegistro(); }, 2000);


                        })
                        .catch(function(error){
                          console.log(error);
                        })
                      })
                      .catch(function(err){
                          console.log("error guardando en citas:"+err);
                      })

                })
                .catch(function(error){
                  console.log(error);
                })

            })
            .catch(function(error) {
              console.log(error);
            });

          })
          .catch(function(error) {
            console.log(error);
            vm.userLogged = false;
          });
      };

      vm.Login = function() {
console.log("Entro aqui por el usuario wwww");
        vm.usuario.ISFACEBOOK = false;

        $auth
          .login(vm.usuario)
          .then(function(result) {
            init();
            vm.showAlertRegistro();
            setTimeout(function(){ vm.closeAlertRegistro(); }, 2000);
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      vm.reloadRoute = function() {

        $window.location = '#/usuario/perfil';
        $window.location.reload();
        alert('Por favor completa tu perfil.');
      }



      vm.MostrarRegistro = function(esProveedor) {
        vm.usuario = {
          Proveedor: esProveedor
        };
        if (esProveedor) {
          $("#nomProveedor").removeAttr('style');
          $("#nomCliente").attr('style','display:none');
        } else{
          $("#nomProveedor").attr('style','display:none');
          $("#nomCliente").removeAttr('style');
        }
        $("#formularioImgs").attr('style','display:none');
        $("#formulario").removeAttr('style');
      }

      vm.MostrarInicioSesion = function() {
        $window.location = '#/usuario/iniciar';
        $window.location.reload();
      }

      vm.formularioImg = function() {
        $("#formulario").attr('style','display:none');
        $("#formularioImgs").removeAttr('style');
      }

      vm.MostrarInicio = function() {
        $window.location = '#/index';
        $window.location.reload();
      }

      vm.showAlert = function() {
        $('#myModal').attr('style','display:table');
      }

      vm.showAlert1 = function() {
        $('#myModal1').attr('style','display:table');
      }

      vm.showAlertRegistro = function() {
        $('#registroExitoso').attr('style','display:table');
      }

      vm.closeAlert = function() {
        $('#myModal').removeAttr('style');
        $('#myModal1').removeAttr('style');
      }

      vm.closeAlertRegistro = function() {
        $('#registroExitoso').removeAttr('style');
        setTimeout(function(){ vm.reloadRoute(); }, 1000);
      }

      init();
      return vm;
    }]);
