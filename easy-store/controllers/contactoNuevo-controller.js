angular
  .module('easy-store')
  .controller('ContactoController', [
    '$scope',
    'UsuarioFactory',
    'PublicacionFactory',
    '$auth',
    '$http',
    '$window',
    function($scope, UsuarioFactory, PublicacionFactory, $auth, $http, $window) {
      var vm = this;

      vm.contactForm         = {};

      vm.SendContactenosMail = function() {
        PublicacionFactory
          .SendContactenosMail(vm.contactForm)
          .then(function(result) {
            alert('Se envio el correo correctamente');
          })
          .catch(function(error) {
            console.log(error);
            alert('Se preseto un error -----------*********');
          });
      };

      function init() {

      };

      init();
      return vm;
    }]);
