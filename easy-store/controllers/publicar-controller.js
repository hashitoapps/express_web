angular
  .module('easy-store')
  .controller('PublicarController', [
    '$scope',
    '$routeParams',
    '$location',
    '$anchorScroll',
    '$sce',
    'PublicacionFactory',
    function(
      $scope,
      $routeParams,
      $location,
      $anchorScroll,
      $sce,
      PublicacionFactory) {
      var vm = this;

      $location.hash('top');
      $anchorScroll();

      PublicacionFactory
        .LoadMostPopulars()
        .then(function(result) {
          vm.mostPopulars = result.data.rows;
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

      return vm;
    }]);
