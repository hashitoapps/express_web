angular
  .module('easy-store')
  .controller('SuscribeController', [
    '$scope',
    'SuscribeFactory',
    function($scope, SuscribeFactory) {
      var vm = this;

      vm.suscribeForm = {};

      vm.Suscribe = function() {
        SuscribeFactory
          .Suscribir(vm.suscribeForm)
          .then(function(result) {
            vm.suscribeForm = {};
            alert('Se registro se ha efectuado de forma exitosa!!!.');
          })
          .catch(function(error) {
            vm.suscribeForm = {};
            console.log(error);
          });
      };

      return vm;
    }]);
