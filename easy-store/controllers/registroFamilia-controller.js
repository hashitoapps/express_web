angular
  .module('easy-store')
  .controller('RegistroFamiliaController', [
    '$scope',
    'UsuarioFactory',
    '$auth',
    '$http',
    '$window',
    function($scope, UsuarioFactory, $auth, $http, $window) {
      var vm = this;

      function init(){
      }

      vm.SignUp = function () {

        console.log(vm.usuario);
        vm.usuario.ISFACEBOOK = false;
        $auth
          .signup(vm.usuario)
          .then(function(result) {
            console.log(vm.usuario);
            $auth.login(vm.usuario);
            vm.showAlertRegistro();
            setTimeout(function(){ vm.closeAlertRegistro(); }, 2000);
          })
          .catch(function(error) {
            console.log(error);
            if (error.data.fields.Correo_UNIQUE) {
              alert('El correo que esta utilizando ya esta registrado.');
            } else {
              alert('Se ha presentado un error al registrar su usuario.');
            }
          });
      };

      vm.FBLogin = function() {
        $auth
          .authenticate('facebook')
          .then(function(result) {
            init();
            vm.showAlertRegistro();
            setTimeout(function(){ vm.closeAlertRegistro(); }, 2000);
          })
          .catch(function(error) {
            console.log('catch');
            console.log(error);

            vm.userLogged = false;
          });
      };

      vm.Login = function() {

        vm.usuario.ISFACEBOOK = false;

        $auth
          .login(vm.usuario)
          .then(function(result) {
            init();
            vm.showAlertRegistro();
            setTimeout(function(){ vm.closeAlertRegistro(); }, 2000);
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      vm.reloadRoute = function() {
        $window.location = '#';
        $window.location.reload();
      }

      vm.MostrarRegistro = function(esProveedor) {
        vm.usuario = {
          Proveedor: esProveedor
        };
        $("#formularioImgs").attr('style','display:none');
        $("#formulario").removeAttr('style');
      }

      vm.MostrarInicioSesion = function() {
        $window.location = '#/usuario/iniciar';
        $window.location.reload();
      }

      vm.formularioImg = function() {
        $("#formulario").attr('style','display:none');
        $("#formularioImgs").removeAttr('style');
      }

      vm.MostrarInicio = function() {
        $window.location = '#';
        $window.location.reload();
      }

      vm.showAlert = function() {
        $('#myModal').attr('style','display:table');
      }

      vm.showAlert1 = function() {
        $('#myModal1').attr('style','display:table');
      }

      vm.showAlertRegistro = function() {
        $('#registroExitoso').attr('style','display:table');
      }

      vm.closeAlert = function() {
        $('#myModal').removeAttr('style');
        $('#myModal1').removeAttr('style');
      }

      vm.closeAlertRegistro = function() {
        $('#registroExitoso').removeAttr('style');
        setTimeout(function(){ vm.reloadRoute(); }, 1000);
      }

      init();
      return vm;
    }]);
