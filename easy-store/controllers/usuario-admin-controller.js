angular
  .module('easy-store')
  .controller('UsuarioAdminController', [
    '$scope',
    '$location',
    '$anchorScroll',
    '$auth',
    '$http',
    'PublicacionFactory',
    'UsuarioFactory',
    '$rootScope',
    function($scope, $location, $anchorScroll, $auth, $http, PublicacionFactory,UsuarioFactory,$rootScope) {
      var vm = this;

      $location.hash('top');
      $anchorScroll();

      vm.EliminarPublicacion = function(publicacion, index) {
        if (confirm([
          '¿Esta seguro que desea eliminar la publicacion: ',
          publicacion.Nombre,
          '?'].join(''))
        ) {
          PublicacionFactory
            .DeletePublicacion(publicacion)
            .then(function(resultado) {
              vm.publicaciones.splice(index, 1);
            })
            .catch(function(error) {
              console.log(error);
            });
        }
      };

      //criferlo
      function loadUser() {
        vm.user          = {};
        vm.files         = [];
        vm.usuario       = {};
        vm.userLogged    = $auth.isAuthenticated();
        vm.contactForm   = {};
        vm.cambiarAvatar = false

        if (vm.userLogged && !$auth.getPayload()) {
          UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {
              console.log("selogueafb");
              vm.user       = result.data;
              console.log("xxxuserid:"+vm.user.Id);
              vm.userLogged = $auth.isAuthenticated();

              vm.user.Telefono         = vm.user.Telefono * 1;
              vm.user.Identificacion   = vm.user.Identificacion * 1;
              vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
              console.log(vm.user);
              //criferlo
              init();
            })
            .catch(function(error) {
              console.log('catch');
              console.log(error);

              vm.userLogged = false;
            });
        } else if(vm.userLogged && !!$auth.getPayload()) {
          vm.user = $auth.getPayload().sub;
          console.log("--------wwwwww",vm.user);

          vm.user.Telefono         = vm.user.Telefono * 1;
          vm.user.Identificacion   = vm.user.Identificacion * 1;
          vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
          //criferlo
          init();
        }

      };

      function init() {

        console.log("aqui inicia");
        vm.publicaciones = [];
        console.log(vm.user.Id);
        PublicacionFactory
          .LoadPublicationsByUserId(vm.user.Id)
          .then(function(result) {
            vm.publicaciones = result.data.rows;

            console.log("publicacionessssssss:"+vm.publicaciones);
          })
          .catch(function(error) {
            console.log(error);
          })
      }
      loadUser();

      return vm;
    }])
  .controller('EditarPublicacionController', [
    '$scope',
    '$routeParams',
    '$timeout',
    '$location',
    '$anchorScroll',
    'PublicacionFactory',
    'SearchFactory',
    'Upload','$q',
    '$auth',
    '$window',
    'UsuarioFactory',
    '$rootScope',
    function($scope, $routeParams, $timeout, $location, $anchorScroll, PublicacionFactory, SearchFactory, Upload,$q,$auth,$window,UsuarioFactory,$rootScope) {
      var vm = this;

      $location.hash('top');
      $anchorScroll();

      function init() {
        var variedades = [];
        var municipios = [];

        vm.file       = {};
        vm.files      = [];
        vm.categorias = [];

        vm.selectedList = [];
        vm.selectedListAux = [];

        vm.optionsList = [];

        vm.seleccionarTodasCiudades = false;

      $rootScope.$on('rootScope:broadcast', function (event, data) {
        init();
      });
          

        SearchFactory
          .LoadCategories()
          .then(function(result) {
            vm.categorias= result.data.rows;
            vm.categorias.forEach(function(cat) {
              variedades = variedades.concat(cat.Variedads);
            });

            console.log("paisSeleccionado"+$rootScope.paisSeleccionado.Id);

            return SearchFactory.LoadDepartamentos($rootScope.paisSeleccionado.Id);

          })
          .then(function(result) {
            vm.departamentos = result.data;

            vm.departamentos.forEach(function(dept) {
              municipios = municipios.concat(dept.Municipios);
            })

            if(!!$routeParams.Id) {
               return PublicacionFactory.LoadPublication($routeParams.Id, true)
            } else {
              return null;
            }
          })          
          .then(function(result) {
            if(result!=null){
                  vm.publicacion               = result.data;
                  vm.publicacion.VariedadIdDb  = vm.publicacion.VariedadId;
                  //vm.publicacion.MunicipioIdDb = vm.publicacion.MunicipioId;
                  vm.publicacion.VariedadId    = "";
                  vm.publicacion.MunicipioId   = "";

                  
                  var variedad = [].filter.call(variedades, function(vari) {
                    return vari.Id == vm.publicacion.VariedadIdDb;
                  })[0];

                  /*var municipio = [].filter.call(municipios, function(muni) {
                    return muni.Id == vm.publicacion.MunicipioIdDb;
                  })[0];*/
                  //vm.CargarMunicipios();

                  $timeout(function() {
                    vm.publicacion.SubCategoriaId = variedad.SubCategoriumId;
                    vm.CargarVariedades();

                    //vm.publicacion.DepartamentoId = municipio.DepartamentoId;
                    

                    $timeout(function() {
                      vm.publicacion.VariedadId  = vm.publicacion.VariedadIdDb;
                      //vm.publicacion.MunicipioId = vm.publicacion.MunicipioIdDb;

                      console.log(vm.publicacion);
                    });
                  });
            
                return PublicacionFactory.
                  ConsultarProductoMunicipioPorProducto(vm.publicacion.Id);
              }else {
                return null;
              }

          })
          .then(function(result){
           if(result!=null){
               console.log(result.data);
                var muns = [];
                muns = result.data.rows;

                vm.selectedList = [];
               muns.forEach(function(current,index,array){
                  vm.selectedList.push({
                    id: current.Municipio.Id,
                    name: current.Municipio.Nombre
                  })
                })
           }
           
            
          })          
          .catch(function(error) {
            console.log(error);
          });

        console.log(vm.files.length);
      };

      vm.EliminarNuevaImagen = function(index) {

        vm.files.splice(index, 1);
      };

      vm.EliminarImagen = function(img, index) {
        PublicacionFactory
          .DeleteImagenPublicacion(img)
          .then(function(result) {
            vm.publicacion.Imagens.splice(index, 1);
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      vm.CargarVariedades = function() {
       
        var categoria = vm.categorias.filter(function(cat) {
          return cat.Id == vm.publicacion.SubCategoriaId;
        });

        vm.variedades             = [];
        vm.publicacion.VariedadId = "";

        if (categoria.length > 0) {
          vm.variedades = categoria[0].Variedads;
        }else{
          alert("Debe seleccionar almenos una categoria.");
        }
        vm.publicacion.VariedadId = categoria[0].Variedads[0].Id;
      }

      vm.CargarMunicipios = function() {
        var departamento = vm.departamentos.filter(function(dept) {
          return dept.Id == vm.publicacion.DepartamentoId;
        });

        vm.municipios = [];
        vm.publicacion.MunicipioId = "";

        if (departamento.length > 0) {
          vm.municipios = departamento[0].Municipios;
        }

        vm.optionsList = [];
        vm.municipios.forEach(function(current,index,array){

          vm.optionsList.push(
          {
              id: current.Id,
              name: current.Nombre
          })   
        });
      }

      vm.GuardarPublicacion = function() {

        if(vm.selectedList.length==0 && vm.selectedListAux.length==0){
            alert("Por favor seleccione al menos una ciudad.")
        }else{
      
        if(vm.seleccionarTodasCiudades ===true){
              vm.selectedList = vm.selectedListAux;
        }
         vm.CargarVariedades();

        var imagesPromises = [];
        var municipioPromises = [];

        vm.publicacion.TipoProducto = 'P';
        vm.publicacion.UsuarioId=vm.user.Id;
        vm.publicacion.MeGusta=0;
        vm.publicacion.NoMeGusta=0;
        vm.publicacion.Estado=0;
        vm.publicacion.MunicipioId = vm.selectedList[0].Id;

        var x=vm.publicacion.Precio;

        if (vm.publicacion.Nombre!=undefined) {
           console.log("Nombre del servicio");
        }else{
          alert("Se debe ingesar nombre del servicio a publicar.");
        }

        //quitar las comas
        if (vm.publicacion.Precio!=undefined) {
           console.log("ENtro aqui xxxxx");
          if (vm.publicacion.Precio!=0) {vm.publicacion.Precio = vm.publicacion.Precio.replace(".","");}
        }else{
          alert("Se debe ingesar el precio del servicio.");
          //vm.publicacion.Precio=0;
        }

        if (vm.publicacion.PrecioDesc!=undefined) {
          if (vm.publicacion.PrecioDesc!=0) {vm.publicacion.PrecioDesc = vm.publicacion.PrecioDesc.replace(".","");}
        }else{
           alert("Se debe ingesar el precio para los clientes de Prevenir Express.");
          
          //vm.publicacion.PrecioDesc=0;
        }
        console.log(vm.publicacion.PorcentajePromo);
        var PP = ""+vm.publicacion.PorcentajePromo; 

        if (vm.publicacion.PorcentajePromo!=undefined) {
          if (vm.publicacion.PorcentajePromo!=0) {vm.publicacion.PorcentajePromo = PP.replace(".","");}
        }else{
         alert("Se debe ingesar el porcentaje de descuento del servicio.");
          //vm.publicacion.PorcentajePromo=0;
        }
        if (vm.publicacion.DescripcionCorta != undefined) {
           console.log("Descripcion corta");
        }else{
          vm.publicacion.DescripcionCorta = "";
        }
        if (vm.publicacion.DescripcionLarga != undefined) {
           console.log("DescripcionLarga");
        }else{
          alert("Se debe ingesar la descripción del servicio a publicar.");
        }
         vm.publicacionresult = {};
         console.log("Imagenes",vm.files.length);

        if(vm.files.length == 0){
          alert("Se debe ingesar almenos una imagen para la publicación.");
          
        }else{

          if(vm.files.length > 6){

          alert("Se debe ingesar máximo 6 imagenes para la publicación.");
        }else{

          PublicacionFactory
          .GuardarPublicacion(vm.publicacion)
          .then(function(result) {
            vm.publicacionresult = result;
            console.log("Resultado publicacion", vm.publicacionresult);

            //selectedList tiene la lista de ciudades selecccionadas
            PublicacionFactory
              .EliminarProductoMunicipio(vm.publicacionresult.data.Id)
              .then(function(result){
                  resolve(result);
              });
              return result;
          })
          .then(function(result){
            [].forEach.call(vm.selectedList, function(elemento) {
              var obj = {
                ProductoId: vm.publicacionresult.data.Id,
                MunicipioId: elemento.id
              }
              var promise = $q(function(resolve, reject) {
              PublicacionFactory
                .GuardarProductoMunicipio(obj)
                .then(function(result) {
                  resolve(result);
                })
                .catch(function(error) {
                  console.log(error);
                  reject(error);
                });
              });

              municipioPromises.push(promise);  
            });
            return $q.all(municipioPromises);
          })  
          .then(function(result) {
            [].forEach.call(vm.files, function(file) {
              var promise = $q(function(resolve, reject) {
                console.log("File", file);
              PublicacionFactory
                .GuardarImagen(vm.publicacionresult, file)
                .then(function(result) {
                  resolve(result);
                })
                .catch(function(error) {
                  console.log(error);
                  reject(error);
                });
              });

              imagesPromises.push(promise);
            });

            return $q.all(imagesPromises);
          })
          .then(function(result) {

            alert("Publicación almacenada correctamente");
            $location.path("/usuario/publicaciones");

            setTimeout(function(){
               vm.SendApprovePublication(result[0].data.ProductoId);
            }, 2000);
          })

          .catch(function(error) {
            console.log(error);
          });
        }//fin else
       }
      };

    }

      vm.SendApprovePublication = function(IdPublicacion) {
        var nombre=vm.publicacion.Nombre.split(" ").join("%20");

        var url = "http://www.prevenirexpress.com/#/aprobacion/"+nombre+"/"+IdPublicacion+"#top";
        console.log("Estas es la url", url);
        var envio = {
          "Correo":vm.user.Correo,
          "Nombre":vm.user.Nombre,
          "Url":url
        }

        PublicacionFactory
          .SendApprovePublication(IdPublicacion, envio)
          .then(function(result) {
            $location.path("/usuario/publicaciones");
          })
          .catch(function(error) {
            console.log(error);
            alert('Se preseto un error');
          });
      };

      vm.ActualizarPublicacion = function() {

          if(vm.selectedList.length==0 && vm.selectedListAux.length==0){
              alert("Por favor seleccione al menos una ciudad.");
          }else{

              var imagesPromises = [];
              var municipioPromises = [];

              vm.publicacion.TipoProducto = 'P';
              vm.publicacion.UsuarioId=vm.user.Id;
              vm.publicacion.Estado=1;
              vm.publicacion.MunicipioId = vm.selectedList[0].Id;
              vm.publicacionresult = {};

              //quitar las comunicandonos

              if ( vm.publicacion.Precio!=0) {
                if (vm.publicacion.Precio.toString().includes(".")) {
                  vm.publicacion.Precio = vm.publicacion.Precio.replace(".","");
                }
              }else{
                vm.publicacion.Precio=0;
              }

              if ( vm.publicacion.PrecioDesc!=0) {
                if (vm.publicacion.PrecioDesc.toString().includes(".")) {
                  vm.publicacion.PrecioDesc = vm.publicacion.PrecioDesc.replace(".","");
                }
              }else{
                vm.publicacion.PrecioDesc=0;
              }

              PublicacionFactory
                .ActualizarPublicacion(vm.publicacion.Id,vm.publicacion)
                .then(function(result){
                  vm.publicacionresult = result;

                   //console.log(result.data.Id);
                    //selectedList tiene la lista de ciudades selecccionadas
                    return PublicacionFactory
                      .EliminarProductoMunicipio(vm.publicacion.Id)
                    

                })
                .then(function(result){
                   [].forEach.call(vm.selectedList, function(elemento) {

                    var obj = {
                      ProductoId: vm.publicacionresult.data.Id,
                      MunicipioId: elemento.id
                    }

                    var promise = $q(function(resolve, reject) {
                    PublicacionFactory
                      .GuardarProductoMunicipio(obj)
                      .then(function(result) {
                        resolve(result);
                      })
                      .catch(function(error) {
                        console.log(error);
                        reject(error);
                      });
                    });

                    municipioPromises.push(promise);  
                  });
                  return $q.all(municipioPromises);
                })  
                .then(function(result) {
                  [].forEach.call(vm.files, function(file) {
                    var promise = $q(function(resolve, reject) {
                    PublicacionFactory
                      .GuardarImagen(vm.publicacionresult, file)
                      .then(function(result) {
                        resolve(result);
                      })
                      .catch(function(error) {
                        console.log(error);
                        reject(error);
                      });
                    });

                    imagesPromises.push(promise);
                  });

                  return $q.all(imagesPromises);
                })
                .then(function(result) {
                  alert("Publicación actualizada correctamente");
                  $urlBase = $location.path();

                  setTimeout(function(){
                     vm.SendApprovePublication(vm.publicacion.Id);
                  }, 2000);

                  $location.path("/usuario/publicaciones");
                })
                .catch(function(error) {
                  console.log(error);
                });
          }      
      };

      //criferlo
      function loadUser() {
        vm.user          = {};
        vm.files         = [];
        vm.usuario       = {};
        vm.userLogged    = $auth.isAuthenticated();
        vm.contactForm   = {};
        vm.cambiarAvatar = false

        if (vm.userLogged && !$auth.getPayload()) {
          UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {
              vm.user       = result.data;
              vm.userLogged = $auth.isAuthenticated();

              vm.user.Telefono         = vm.user.Telefono * 1;
              vm.user.Identificacion   = vm.user.Identificacion * 1;
              vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
            })
            .catch(function(error) {
              console.log('catch');
              console.log(error);

              vm.userLogged = false;
            });
        } else if(vm.userLogged && !!$auth.getPayload()) {
          vm.user = $auth.getPayload().sub;

          vm.user.Telefono         = vm.user.Telefono * 1;
          vm.user.Identificacion   = vm.user.Identificacion * 1;
          vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
        }
        console.log("user:"+vm.user.Id);
      };

      vm.seleccionarTodas = function(){
        if(vm.seleccionarTodasCiudades ===false){
          vm.CargarMunicipios();
          vm.selectedList =[];
        }else if(vm.seleccionarTodasCiudades ===true){
          vm.selectedList = [];
          vm.selectedListAux = [];
          vm.optionsList.forEach(function(current,index,array){

          vm.selectedListAux.push(
          {
              id: current.id,
              name: current.name
          })   
        });
        }
      }

      loadUser();

      init();

      return vm;
    }])
  .controller('PerfilUsuarioController' , [
    '$scope',
    '$auth',
    '$routeParams',
    '$timeout',
    '$location',
    '$anchorScroll',
    'PublicacionFactory',
    'SearchFactory',
    'Upload',
    'UsuarioFactory',
    '$window',
    function($scope, $auth, $routeParams, $timeout, $location, $anchorScroll, PublicacionFactory, SearchFactory, Upload,UsuarioFactory,$window) {
      var vm = this;
      vm.paises = [];
      vm.paisSeleccionado = {};
      vm.user = {};

      // Upload.base64DataUrl(temp2[0]).then(function(result){imgB64=result;});

      

      $scope.$watch('vm.files', function() {
        console.log(vm.files);

        vm.cambiarAvatar = false;
      });

       vm.LoadPaises = function(pais){

        UsuarioFactory.CargarPaises()
        .then(function(result){
          
          vm.paises = result.data.rows; 
          vm.paisSeleccionado = pais;///Colombia
        });
      }

      function init() {
        //vm.user          = {};
        vm.files         = [];
        vm.usuario       = {};
        vm.userLogged    = $auth.isAuthenticated();
        vm.contactForm   = {};
        vm.cambiarAvatar = false

        if (vm.userLogged && !$auth.getPayload()) {
          UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {

              vm.user       = result.data;
              console.log("..........................."+vm.user.Proveedor);
              console.log(vm.user);
              vm.userLogged = $auth.isAuthenticated();

              vm.user.Telefono         = vm.user.Telefono * 1;
              vm.user.Identificacion   = vm.user.Identificacion * 1;
              vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;
              vm.LoadPaises(vm.user.Pai);

            })
            .catch(function(error) {
              console.log('catch');
              console.log(error);
              vm.userLogged = false;
            });
        } else if(vm.userLogged && !!$auth.getPayload()) {
          vm.user = $auth.getPayload().sub;

          vm.user.Telefono         = vm.user.Telefono * 1;
          vm.user.Identificacion   = vm.user.Identificacion * 1;
          vm.user.TelefonoWhatsApp = vm.user.TelefonoWhatsApp * 1;

          var data = {email: vm.user.Correo}
          UsuarioFactory
            .ObtenerUsuarioPorEmail(data)
            .then(function(result){
               vm.user = result.data;
               vm.LoadPaises(vm.user.Pai);
               console.log("ojo aqui:"+vm.user.UrlPagina+" "+vm.user.UrlIntroduccion);
            })
            .catch(function(err){
              console.log(err);
            });
        
        }

        console.log(vm.user);
      };

      vm.Logout = function() {
        $auth.logout();
        vm.userLogged = false;
        setTimeout(function(){ vm.reloadRoute(); }, 1000);
      };

      vm.reloadRoute = function() {
          $window.location = '#usuario/iniciar';
          $window.location.reload();
      }

      vm.GuardarPerfil = function() {
          vm.user.PaiId = vm.paisSeleccionado.Id;
          UsuarioFactory.Guardar(vm.user.Id,vm.user)
          .then(function(result){
              vm.user = result.data;              
              alert('Se almacenó los datos exitosamente. Los cambios se verán reflejados en el próximo inicio de sesión');
              vm.Logout();
          })
          .catch(function(error){
              console.log(error);
          })

      };

      init();

      return vm;
    }])
  .controller('NuevaCategoriaController', ['$scope', function($scope) {
    $scope.nvaCategoriaForm = {};

    $scope.RequestCategory = function() {
      alert('En breve estaremos comunicandonos con usted, gracias por utilizar nuestros servicios');
      $scope.closeThisDialog('');
    }
    }
  ]);

  function manito(id){
      if (id=="megusta") {
        $('#megusta').attr('style','color:black');
        $('#megusta').addClass('megusta');
        $('#nomegusta').removeAttr('style');
        $('#nomegusta').removeClass('nomegusta');

      } else if (id=="nomegusta") {
        $('#megusta').removeAttr('style');
        $('#megusta').removeClass('megusta');
        $('#nomegusta').attr('style','color:black');
        $('#nomegusta').addClass('nomegusta');
      }

      $('#enviar').trigger('click');
    }

  function format(id){
    var valor=$("#"+id).val();
    valor = valor.split(".").join(",");
    var x=accounting.formatMoney(valor, "", "0");
    x = x.split(",").join(".");
    $("#"+id).val(x);
  }

  function validarPorcentaje(id){
    var valor=$("#"+id).val();
    if (valor>100) {
      $("#"+id).val('0');
      alert('El valor no puede superar el 100%');
    }
  }
