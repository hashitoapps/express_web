angular
  .module('easy-store')
  .controller('PublicacionController', [
    '$scope',
    '$routeParams',
    '$location',
    '$anchorScroll',
    '$sce',
    'PublicacionFactory',
    'SearchFactory',
    '$auth',
    'UsuarioFactory',
    '$timeout',
    function($scope, $routeParams, $location, $anchorScroll,
      $sce, PublicacionFactory, SearchFactory, $auth, UsuarioFactory, $timeout) {
      var vm = this;

      vm.options = {
        $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
        $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
        $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
        $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value i
        $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
        $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
        $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
        $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
        //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
        //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
        $SlideSpacing: 0,                                   //[Optional] Space between each slide in pixels, default value is 0
        $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
        $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
        $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
        $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
        $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not
        // $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
        //   $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
        //   $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
        //   $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
        //   $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
        // },
        $BulletNavigatorOptions: {                        //[Optional] Options to specify and enable navigator or not
          $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
          $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
          $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
          $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
          $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
          $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
          $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
          $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
        },
        $ArrowNavigatorOptions: {                         //[Optional] Options to specify and enable arrow navigator or not
          $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
          $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
          $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
          $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
        }
      };

      vm.contactForm         = {};
      vm.commentForm         = {};
      vm.mostPopulars        = [];
      vm.currentPublication  = {};
      vm.similarPublications = [];
      vm.comentario          = {};
      vm.aprobarPublicacion    = {};
      vm.comentarios ={};
      vm.usuarioCitasId = null;
      vm.usuarioPublicacionCitasId =null;

      vm.CargarComentarios = function() {
        console.log("ID publicacion",$routeParams.Id);
        SearchFactory
          .LoadComentarios($routeParams.Id)
          .then(function(result) {
            vm.comentarios = result.data.rows;
            console.log("Comentarios disponibles",vm.comentarios);
          })
          .catch(function(error) {
            console.log(error);
          });
      };

      vm.SendContactMail = function() {
        PublicacionFactory
          .SendContactMail($routeParams.Id, vm.contactForm)
          .then(function(result) {
            //vm.currentPublication = result;
            alert("El mensaje fue enviado exitosamente..");
            vm.contactForm.Name = "";
            vm.contactForm.Email = "";
            vm.contactForm.Message = "";
            console.log(" currentPublication", vm.currentPublication);
          })
          .catch(function(error) {
            console.log(error);
            alert('Se preseto un error ********');
          });
      };

      PublicacionFactory
        .LoadMostPopulars()
        .then(function(result) {
          vm.mostPopulars = result.data.rows;
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

      PublicacionFactory
        .LoadSimilarPublications($routeParams.Id)
        .then(function(result) {
          vm.similarPublications = result.data;
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

      PublicacionFactory
        .LoadPublication($routeParams.Id)
        .then(function(result) {
          vm.currentPublication = result.data;

          console.log("Entro a qui a publicacion");

          //console.log(vm.currentPublication);

          vm.currentPublication.Usuario.Ubicacion =
            $sce.trustAsHtml(vm.currentPublication.Usuario.Ubicacion);
          vm.currentPublication.Usuario.UrlIntroduccion =
            $sce.trustAsHtml(vm.currentPublication.Usuario.UrlIntroduccion);

            //vm.link = vm.currentPublication.Usuario.UrlIntroduccion;
            var linkstring = vm.currentPublication.Usuario.UrlIntroduccion;
            if(linkstring){
                vm.link = $sce.trustAsHtml(linkstring.toString());
            }


          $location.hash('top');
          $anchorScroll();

          //consulta al usuario para obtener el UsuarioCitasId
            UsuarioFactory
              .ObtenerUsuarioPorEmail({email: vm.currentPublication.Usuario.Correo})
              .then(function(res){
                //console.log("usucitasid"+res.data.UsuarioCitasId);
                console.log(res.data);
                vm.usuarioPublicacionCitasId = res.data.UsuarioCitasId;
              })
              .catch(function(err){
                console.log(err);
              })


        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

        vm.SendCommentMail = function(ComentarioId) {
          vm.commentForm.Comentario=vm.currentPublication.Comentario;
          vm.commentForm.IdPublicacion=vm.currentPublication.Id;

          var envio = {
            "Correo":vm.currentPublication.Usuario.Correo,
            "Comentario":vm.commentForm.Comentario,
            "Origen": [vm.user.Nombre,vm.user.Correo].join("-")
          }

          console.log("envio"+envio);

          PublicacionFactory
            .SendCommentMail(ComentarioId, envio)
            .then(function(result) {
              vm.currentPublication = {};
              //$location.path("/index");
            })
            .catch(function(error) {
              console.log(error);
              alert('Se preseto un error');
            });
        };

        vm.CrearComentario = function()
        {
          vm.comentario.Comentario=vm.currentPublication.Comentario;
          vm.comentario.IdPublicacion=vm.currentPublication.Id;

          PublicacionFactory
          .GuardarComentario(vm.comentario)
          .then(function(result) {
            alert("Comentario enviado correctamente.");
           
            console.log("comentario:"+result.data.Id);
            vm.SendCommentMail(result.data.Id);
            vm.CargarComentarios()
          })
          .catch(function(error) {
            console.log(error);
          });
        }


        vm.AcutalizarMeGusta = function()
        {
          if ( $('#megusta').hasClass('megusta')) {
            vm.currentPublication.MeGusta=vm.currentPublication.MeGusta+1;
          }else if ($('#nomegusta').hasClass('nomegusta')) {
            vm.currentPublication.NoMeGusta=vm.currentPublication.NoMeGusta+1;
          }

          PublicacionFactory
          .ActualizarPublicacion(vm.currentPublication.Id,vm.currentPublication)
          .then(function(result) {
            $urlBase = $location.path();

            if ( $('#megusta').hasClass('megusta')) {
            alert("La publicacion te gusta");
            }else if ($('#nomegusta').hasClass('nomegusta')) {
              alert("La publicacion no te gusta");
            }

            $('#megusta, #nomegusta').removeAttr('style');
            $('#megusta').removeClass('megusta');
            $('#nomegusta').removeClass('nomegusta');
          })
          .catch(function(error) {
            console.log(error);
          });
        }

        vm.closeAlertRegistro = function() {
          $('#popupCalificacion').removeAttr('style');
        }

        function init(){
          

          $timeout(function () {
            console.log("Entro a quissss");
              vm.CargarComentarios();
              $("#popupCalificacion").attr('style','display:table');
          }, 500);


          vm.user        = {};
          vm.usuario     = {};
          vm.userLogged  = $auth.isAuthenticated();

          if (vm.userLogged && !$auth.getPayload()) {
            UsuarioFactory
              .InfoUserFB($auth.getToken())
              .then(function(result) {
                vm.user       = result.data;
                vm.userLogged = $auth.isAuthenticated();

                //consulta al usuario para obtener el UsuarioCitasId
                  UsuarioFactory
                    .ObtenerUsuarioPorEmail({email: vm.user.Correo})
                    .then(function(res){
                      console.log("usucitasid"+res.data.UsuarioCitasI);
                      vm.usuarioCitasId = res.data.UsuarioCitasId;
                      setTimeout(function(){ vm.reloadRoute(); }, 2000);
                    })
                    .catch(function(err){
                      console.log(err);
                    })


              })
              .catch(function(error) {
                console.log('catch');
                console.log(error);

                vm.userLogged = false;
              });
          } else if(vm.userLogged && !!$auth.getPayload()){
            vm.user = $auth.getPayload().sub;

          }

          //consulta al usuario para obtener el UsuarioCitasId
            UsuarioFactory
              .ObtenerUsuarioPorEmail({email: vm.user.Correo})
              .then(function(res){
                console.log("usucitasid"+res.data.UsuarioCitasI);
                vm.usuarioCitasId = res.data.UsuarioCitasId;
              })
              .catch(function(err){
                console.log(err);
              })
        }
        init();

      return vm;
    }]);
