angular
  .module('easy-store')
  .controller('ListController', [
    '$scope',
    '$routeParams',
    '$location',
    '$anchorScroll',
    'SearchFactory',
    'PublicacionFactory',
    function(
      $scope,
      $routeParams,
      $location,
      $anchorScroll,
      SearchFactory,
      PublicacionFactory) {
      var vm = this;

      vm.categorias    = [];
      vm.subCategorias = [];
      vm.variedades    = [];

      vm.publicationList        = [];
      vm.publicationCount       = 0;
      vm.page                   = $routeParams.page || 1;
      vm.pageSize               = 10;
      vm.orderBy                = 'MeGusta';
      vm.sortOrder              = 'DESC';
      vm.pages                  = [];
      vm.filters                = {};
      vm.categoryId             = {};
      vm.SubCategoria1          = {};
      vm.tipo = true;

      console.log("Que parametro me llega",$routeParams);
      
      vm.consultaUnaCategoria = function() {
      SearchFactory
      .LoadCategoriesId($routeParams.categoriaId)
      .then(function(result){
        vm.categoryId = result.data;
        console.log("Categoria ID",vm.categoryId);

      }).catch(function(error) {
          console.log(error);
          ////alert('Se presento un error');
        });
      }

     
      SearchFactory
        .LoadCategories()
        .then(function(result) {
          vm.categorias = result.data.rows;
          console.log("Todas las categorias" , vm.categorias);

          vm.subCategorias    = vm.categorias[0].SubCategoria;
          vm.filters.MinValue = parseInt($routeParams.min);
          vm.filters.MaxValue = parseInt($routeParams.max); 
          vm.consultaUnaCategoria();
          console.log($routeParams.IdMunicipio);
          
          setTimeout(function() {

            vm.filters.IdCategoria    = vm.categorias[0].Id;
            vm.filters.IdSubCategoria = $routeParams.subCategoriaId;
            vm.filters.IdMunicipio = $routeParams.IdMunicipio;

            vm.filters.IdVariedad = vm.categoryId.Variedads[0].Id;

            vm.LoadVarieties();

            $scope.$apply();
            
            vm.filters.IdVariedad = vm.categoryId.Variedads[0].Id;

            console.log("routeparams:"+$routeParams);

            vm.filters.Descripcion = $routeParams.Descripcion;

            $scope.$apply();

            vm.LoadPublicationList(vm.page);

          }, 1000);
        })
        .catch(function(error) {
          console.log(error);
          ////alert('Se presento un error');
        });

        vm.LoadSubCategories = function() {
          var category = [].filter.call(vm.categorias, function(item) {
            return item.Id == vm.IdCategory;
          })[0];

          vm.IdSubCategory = undefined;
          vm.IdVariety     = undefined;
          vm.subCategorias = $routeParams.subCategoriaId;
          vm.subCategorias = vm.categorias;
        };



        vm.LoadVarieties = function() {
          var category = [].filter.call(vm.categorias, function(item) {
            return item.Id == vm.IdCategory;
          })[0];

          //var subCategory = [].filter.call(category, function(item) {
            //return item.Id == vm.IdSubCategory;
          //})[0];

          vm.IdVariety  = 1;
          //vm.variedades = category.Variedads;
        };

      vm.LoadPublicationList = function(page) {
        var filter = [];

        if (vm.filters.IdCategoria) {
          filter.push('IdCategoria=' + vm.filters.IdCategoria);
        }

        if (vm.filters.IdSubCategoria) {
          filter.push('IdSubCategoria=' + vm.filters.IdSubCategoria);
        }

        if (vm.filters.IdVariedad) {
          filter.push('IdVariedad=' + vm.filters.IdVariedad);
        }

        if (vm.filters.Descripcion) {
          filter.push('Descripcion=' + vm.filters.Descripcion);
        }

        if (vm.filters.MinValue) {
          filter.push('MinValue=' + vm.filters.MinValue);
        }

        if (vm.filters.MaxValue) {
          filter.push('MaxValue=' + vm.filters.MaxValue);
        }

        if(vm.filters.IdMunicipio){
          filter.push('IdMunicipio='+vm.filters.IdMunicipio);
        }
        
        console.log("esto es filter xxxxx:",vm.filters);


        console.log("esto es filter:"+filter.join('%'));

        PublicacionFactory
          .LoadPublicationList(
            page,
            vm.pageSize,
            vm.orderBy,
            vm.sortOrder,
            filter.join('&')
          )
          .then(function(model) {
            vm.publicationList  = model.data.rows;
            vm.publicationCount = model.data.count;
            vm.pages            = [];

            var numPages = Math.trunc(vm.publicationCount / vm.pageSize);

            numPages = vm.publicationCount % vm.pageSize === 0 ?
              numPages : numPages + 1;

            for (var i = 1; i <= numPages; i++) {
              vm.pages.push(i);
            }

            $location.hash('top');
            $anchorScroll();
          })
          .catch(function(error) {
            console.log(error);

          });
      };

      vm.pageShow = function(item) {
        var pagerSize = 10;
        var end = vm.page + (pagerSize / 2);

        end = end > vm.pages.length ? vm.pages.length : end;

        var start = end - pagerSize + 1;

        start = start < 1 ? 1 : start;

        end = start === 1 ? pagerSize : end;

        return start <= item && item <= end;
      };

      vm.ChangePage = function(page) {
        var urlBase = $location.path().split('/page')[0];

        $location.path([urlBase, 'page', page].join('/'));
      };

      vm.Filter = function() {
        var filter  = [];
        var filter2 = [];

        if (vm.filters.IdSubCategoria) {
          filter.push('/resultados/' + vm.filters.IdSubCategoria);
        }

        if (vm.filters.IdVariedad) {
          filter.push('/' + vm.filters.IdVariedad);
        }

        filter.push('/page/1');

        if (vm.filters.MinValue) {
          filter2.push('min=' + vm.filters.MinValue);
        }

        if (vm.filters.MaxValue) {
          filter2.push('max=' + vm.filters.MaxValue);
        }

        var url = filter.join('');

        if (filter2.length > 0) {
          url = [url, '?', filter2.join('&')].join('');
        }

        location.href = '#' + url;
      };

      return vm;
    }]);
