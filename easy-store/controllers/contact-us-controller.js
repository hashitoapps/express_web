angular
  .module('easy-store')
  .controller('ContactUsController', [
    '$scope',
    'UsuarioFactory',
    '$auth',
    '$http',
    '$window',
    '$rootScope',
    function($scope, UsuarioFactory, $auth, $http, $window,$rootScope) {
      var vm = this;
      vm.paises = [];
      vm.hola = "holasss";
      vm.paisSeleccionado={};
      vm.paisesX = [];

      //console.log($auth);

      vm.CambiaPais = function(){
        console.log(vm.paisSeleccionado);
        $rootScope.paisSeleccionado = vm.paisSeleccionado;
        $rootScope.$broadcast('rootScope:broadcast', $rootScope.paisSeleccionado);
      }

      vm.LoadPaises = function(pais){


        UsuarioFactory.CargarPaises()
        .then(function(result){

          vm.paises = result.data.rows;
          vm.paisSeleccionado = pais;///Colombia
          $rootScope.paisSeleccionado = pais;
          $rootScope.$broadcast('rootScope:broadcast', $rootScope.paisSeleccionado);

        });
      }

      vm.SendMessage = function() {
        $scope.contactForm = {};
        $scope.closeThisDialog('');
      };

      vm.FBLogin = function() {
        $auth
          .authenticate('facebook')
          .then(function(result) {
            init();
          })
          .catch(function(error) {
            console.log('catch');
            console.log(error);

            vm.userLogged = false;
          });
      };

      vm.Logout = function() {
        $auth.logout();
        vm.userLogged = false;
        setTimeout(function(){ vm.reloadRoute(); }, 1000);
      };

      vm.Login = function() {
        console.log("Estoy entrnado por login");
        vm.usuario.ISFACEBOOK = false;

        $auth
          .login(vm.usuario)
          .then(function(result) {
            init();
          })
          .catch(function(error) {
            console.log(error);
          });
      }

      function init() {
        vm.user        = {};
        vm.usuario     = {};
        vm.userLogged  = $auth.isAuthenticated();
        vm.contactForm = {};
        var correoBuscar = "";

        if (vm.userLogged && !$auth.getPayload()) {
          UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {
              vm.user       = result.data;
              correoBuscar = vm.user.Correo;
              vm.userLogged = $auth.isAuthenticated();
              console.log("porface"+vm.user.Pai.Id);
              vm.LoadPaises(vm.user.Pai);

              UsuarioFactory
                .ObtenerUsuarioPorEmail({email: correoBuscar})
                .then(function(res){
                  console.log(res.data.UsuarioCitasId);
                  vm.usuarioCitasId = res.data.UsuarioCitasId;
                })
                .catch(function(err){
                  console.log(err);
                })
            })
            .catch(function(error) {
              console.log('catch');
              console.log(error);


              vm.userLogged = false;
            });
        } else if(vm.userLogged && !!$auth.getPayload()) {
          vm.user = $auth.getPayload().sub;
          correoBuscar = vm.user.Correo;
          //console.log("porotro"+vm.user.Pai.Nombre);
          vm.LoadPaises(vm.user.Pai);
        }

        //consulta al usuario para obtener el UsuarioCitasId
        if(correoBuscar!==""){
          UsuarioFactory
            .ObtenerUsuarioPorEmail({email: correoBuscar})
            .then(function(res){
              console.log(res.data.UsuarioCitasId);
              vm.usuarioCitasId = res.data.UsuarioCitasId;
            })
            .catch(function(err){
              console.log(err);
            })
        }
      };


      vm.reloadRoute = function() {
          $window.location = '#';
          $window.location.reload();
      }

      init();
      return vm;
      console.log("Esto en cosntant us controller");
    }]);
