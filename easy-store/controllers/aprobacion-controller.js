angular
  .module('easy-store')
  .controller('AprobacionController', [
    '$scope',
    '$routeParams',
    '$location',
    '$anchorScroll',
    '$sce',
    'PublicacionFactory',
    function($scope, $routeParams, $location, $anchorScroll,
      $sce, PublicacionFactory) {
      var vm = this;

      vm.options = {
        $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
        $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
        $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
        $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value i
        $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
        $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
        $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
        $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
        //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
        //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
        $SlideSpacing: 0,                                   //[Optional] Space between each slide in pixels, default value is 0
        $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
        $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
        $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
        $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
        $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not
        // $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
        //   $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
        //   $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
        //   $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
        //   $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
        // },
        $BulletNavigatorOptions: {                        //[Optional] Options to specify and enable navigator or not
          $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
          $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
          $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
          $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
          $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
          $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
          $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
          $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
        },
        $ArrowNavigatorOptions: {                         //[Optional] Options to specify and enable arrow navigator or not
          $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
          $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
          $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
          $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
        }
      };

      vm.contactForm         = {};
      vm.mostPopulars        = [];
      vm.currentPublication  = {};
      vm.similarPublications = [];
      vm.comentario          = {};
      vm.resultForm          = {};

      PublicacionFactory
        .LoadMostPopulars()
        .then(function(result) {
          vm.mostPopulars = result.data.rows;
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

      PublicacionFactory
        .LoadSimilarPublications($routeParams.Id)
        .then(function(result) {
          vm.similarPublications = result.data;
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

      PublicacionFactory
        .LoadPublication($routeParams.Id)
        .then(function(result) {
          vm.currentPublication = result.data;
          vm.currentPublication.Usuario.Ubicacion =
            $sce.trustAsHtml(vm.currentPublication.Usuario.Ubicacion);
          vm.currentPublication.Usuario.UrlIntroduccion =
            $sce.trustAsHtml(vm.currentPublication.Usuario.UrlIntroduccion);

            vm.link = vm.currentPublication.Usuario.UrlIntroduccion;

          $location.hash('top');
          $anchorScroll();

          console.log(vm.currentPublication);
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

        vm.ActualizarEstado = function()
        {
          if ( $('#aprobado').hasClass('aprobado')) {
            vm.currentPublication.Estado=1;
          }else if ($('#desaprobado').hasClass('desaprobado')) {
            vm.currentPublication.Estado=0;
          }


          console.log("Entro a actualizar");
            PublicacionFactory
            .ActualizarPublicacion(vm.currentPublication.Id,vm.currentPublication)
            .then(function(result) {
              $urlBase = $location.path();

                if ( $('#aprobado').hasClass('aprobado')) {
                  alert("La publicación ha sido aprobada");
                  vm.resultForm.Razon="Su publicación ha sido aprobada ";

                }else if ($('#desaprobado').hasClass('desaprobado')) {
                  alert("La publicación no ha sido aprobada");
                  vm.resultForm.Razon="Su publicación no ha sido aprobada ";
                }
                vm.SendResultApproveMail();

                $('#aprobado, #desaprobado').removeAttr('style');
                $('#aprobado').removeClass('aprobado');
                $('#desaprobado').removeClass('desaprobado');

                $location.path("/index");
                })
                .catch(function(error) {
                  console.log(error);
                });

        }

        vm.SendResultApproveMail = function() {
          console.log("Entro para comprobar una publicacion");
          vm.resultForm.Comentario=vm.currentPublication.ComentarioRazon
          vm.resultForm.IdPublicacion=vm.currentPublication.Id;

          var envio = {
            "Correo":vm.currentPublication.Usuario.Correo,
            "Razon":vm.resultForm.Razon,
            "Comentario": vm.resultForm.Comentario
          }

          PublicacionFactory
            .SendResultApproveMail(envio)
            .then(function(result) {
              vm.currentPublication = {};
              $location.path("/index");
            })
            .catch(function(error) {
              console.log(error);
              alert('Se preseto un error ----------');
            });
        };

      return vm;
    }]);


    function clase(id){
        if (id=="aprobado") {
          $('#aprobado').attr('style','color:black');
          $('#aprobado').addClass('aprobado');
          $('#desaprobado').removeAttr('style');
          $('#desaprobado').removeClass('desaprobado');

          $('#enviarApro').trigger('click');

        } else if (id=="desaprobado") {
          $('#aprobado').removeAttr('style');
          $('#aprobado').removeClass('aprobado');
          $('#desaprobado').attr('style','color:black');
          $('#desaprobado').addClass('desaprobado');

          if ($('#texto').val()!="") {
            $('#enviarApro').trigger('click');
          }
          else {
            alert('Debe escribir razón por la cual se rechaza esta publicación');
          }
        }

      }
