/**
* easy-store Module
*
* SearchFactory, se encarga de cargar toda la información de categorias, subcategorias y variedades
*/
angular
  .module('easy-store')
  .controller('SearchController', [
    '$scope',
    '$location',
    'SearchFactory',
    '$rootScope',
    function($scope, $location, SearchFactory,$rootScope) {
      var vm = this;

      vm.categorias    = [];
      vm.subCategorias = [];
      vm.variedades    = [];
      vm.IdCategory    = undefined;
      vm.IdSubCategory = undefined;
      vm.IdVariety     = undefined;
      vm.Descripcion     = undefined;
      vm.departamentos = [];
      vm.departamentoSeleccionado = {};
      vm.ciudadSeleccionada = {};
       vm.categoryId        = {};

      $rootScope.$on('rootScope:broadcast', function (event, data) {
        vm.LoadDepartamentos();
      });

      SearchFactory
        .LoadCategories()
        .then(function(result) {
          vm.categorias = result.data.rows;
          //vm.IdCategory = 2;
          vm.LoadSubCategories();
        })
        .catch(function(error) {
          console.log(error);
          //alert('Se presento un error');
        });

      vm.LoadDepartamentos = function(){
        console.log($rootScope.paisSeleccionado);
         SearchFactory.LoadDepartamentos($rootScope.paisSeleccionado.Id)
         .then(function(result){
            //console.log(result.data);
            vm.ciudadSeleccionada = {};
            vm.departamentos = [];
            vm.departamentos = result.data;
            vm.departamentoSeleccionado = vm.departamentos[0];
            vm.LoadMunicipios();
            
            
         })
         //console.log("Entro por SearchController")
      }  

      vm.LoadMunicipios = function(){
        console.log(vm.departamentoSeleccionado);
        vm.municipios = [];
        vm.municipios = vm.departamentoSeleccionado.Municipios;        

        vm.optionsList = [];
        vm.municipios.forEach(function(current,index,array){

          vm.optionsList.push(
          {
              id: current.Id,
              name: current.Nombre
          })   
        });


        vm.ciudadSeleccionada =  vm.municipios[0];
        //console.log("Entro por SearchController <<<<<<<<")
      }

      vm.LoadSubCategories = function() {
        var category = [].filter.call(vm.categorias, function(item) {
          return item.Id == vm.IdCategory;
        })[0];

        vm.IdSubCategory = undefined;
        vm.IdVariety     = undefined;
        //vm.subCategorias = category.SubCategoria;
        vm.subCategorias = vm.categorias;
        //console.log("Entro por SearchController");
      };

      vm.LoadVarieties = function() {
        var category = [].filter.call(vm.categorias, function(item) {
          return item.Id == vm.IdCategory;
        })[0];

        vm.IdVariety  = undefined;
        //vm.variedades = category.Variedads;
        console.log("Entro por SearchController", vm.IdSubCategory);
      };

      vm.Search = function() {
        //alert(vm.ciudadSeleccionada.Id);
        console.log("Entro por SearchControllerrrrrr")
        if (vm.IdSubCategory !== undefined && vm.IdSubCategory !== null && vm.Descripcion == undefined && vm.Descripcion == null) {
          //alert("1");
          var path = [ '/resultados', vm.IdSubCategory,vm.ciudadSeleccionada.Id].join('/');
          $location.path(path + '/page/1');
        }
        else if (vm.IdSubCategory !== undefined && vm.Descripcion !== undefined) {
          //alert("2xxxxxxxxx",vm.Descripcion);

          var path = [ '/resultados',vm.IdSubCategory].join('/');
          var path = [ path, vm.IdSubCategory].join('/');
          var path = [path, 1].join('/');
          var path = [ path, vm.Descripcion].join('/');
          var path = [ path, vm.ciudadSeleccionada.Id].join('/');
          $location.path(path + '/page/1');
        }else if (vm.IdSubCategory == undefined && vm.Descripcion !== undefined) {
          //alert("3");
          var path = [ '/resultados',vm.IdSubCategory].join('/');
          var path = [ path, '0'].join('/');
          var path = [ path, '0'].join('/');
          var path = [ path, vm.Descripcion].in('/');
          var path = [ path, vm.ciudadSeleccionada.Id].join('/');
          $location.path(path + '/page/1');
          console.log(path+'/page/1');
        }
      }

      setTimeout(function(){ vm.LoadDepartamentos(); }, 1000);

      
      return vm;
    }]);
