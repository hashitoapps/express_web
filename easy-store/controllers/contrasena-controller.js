angular
  .module('easy-store')
  .controller('ContrasenaController', [
    '$scope',
    '$routeParams',
    'UsuarioFactory',
    '$auth',
    '$http',
    '$window',
    function($scope, $routeParams,UsuarioFactory, $auth, $http, $window) {
      var vm = this;
      var emailusuario = null;
      vm.contrasena1=null;
      vm.contrasena2=null;
      vm.user = null;
      vm.emailformulario = null;
     

      function init(){
        console.log("hola");

        var encriptado = {email: $routeParams.Id};

        UsuarioFactory.
         ObtenerDes(encriptado)
         .then(function(res){
            //obtiene en data.data el email de la persona
            emailusuario = res.data.data; 

            var envio = {email: emailusuario};

            UsuarioFactory.
            ObtenerUsuarioPorEmail(envio)
            .then(function(res){
              vm.user = res.data;
              console.log(vm.user);
            })
            .catch(function(err){
              console.log("error:"+err);
            })

         })
         .catch(function(err){
            console.log("error:"+err);
         })

      }

      vm.ChangePassword = function(){
        console.log("vm.contrasena1",vm.contrasena1);
        if(vm.contrasena1 != vm.contrasena2){
          alert("Las contraseñas no coinciden.");
          console.log("vm.contrasena2",vm.user.Clave);
        }else{
         // console.log("vm.contrasena1",vm.contrasena2);
           vm.user.Clave = vm.contrasena2;
           UsuarioFactory.
           Guardar(vm.user.Id,vm.user) 
           .then(function(res){
              console.log(res);
              alert("Se actualizó correctamente la contraseña");
              $window.location = '#/usuario/iniciar';
              $window.location.reload();
           })
           .catch(function(err){
              console.log("error:"+err);
           })
        }
      }

      vm.EnviarCorreo = function(){
        var data = {email: vm.emailusuario};
        UsuarioFactory.
           RecuperarContra(data)
           .then(function(res){
              console.log(res.data);
              alert("Se ha enviado las instrucciones a su correo");
              $window.location = '#/';
           })
           .catch(function(err){
              console.log(err);
           })
      }
    

      init();
      return vm;
  
  }]);