angular
  .module('easy-store')
  .controller('SesionController', [
    '$scope',
    'UsuarioFactory',
    '$auth',
    '$http',
    '$window',
    function($scope, UsuarioFactory, $auth, $http, $window) {
      var vm = this;

      vm.SendMessage = function() {
        $scope.contactForm = {};
        $scope.closeThisDialog('');
      };

      vm.FBLogin = function() {
        $auth
          .authenticate('facebook')
          .then(function(result) {
            init();
          })
          .catch(function(error) {
            console.log('catch');
            console.log(error);

            vm.userLogged = false;
          });
      };

      vm.Login = function() {

        vm.usuario.ISFACEBOOK = false;
        console.log("Entro aqui por el usuario");
        $auth
          .login(vm.usuario)
          .then(function(result) {
            //console.log(result);
            init();
          })
          .catch(function(error) {
            //console.log("error:"+error);
            alert(error.data);
          });
      }

      vm.Logout = function() {
        $auth.logout();
        vm.userLogged = false;
      };

      function init(){
        console.log("Entro aqui por el usuario");
        vm.user        = {};
        vm.usuario     = {};
        vm.userLogged  = $auth.isAuthenticated();
        vm.contactForm = {};
        //vm.terminos = "checked";

        if (vm.userLogged && !$auth.getPayload()) {
          UsuarioFactory
            .InfoUserFB($auth.getToken())
            .then(function(result) {
              vm.user       = result.data;
              vm.userLogged = $auth.isAuthenticated();

              setTimeout(function(){ vm.reloadRoute(); }, 2000);
            })
            .catch(function(error) {
              console.log('catch');
              console.log(error);

              vm.userLogged = false;
            });
        } else if(vm.userLogged && !!$auth.getPayload()){
          vm.user = $auth.getPayload().sub;

          var data = {email: vm.user.Correo};

          UsuarioFactory
           .ObtenerUsuarioPorEmail(data)
           .then(function(result){
              vm.user = result.data;
              //vm.LoadPaises(vm.user.Pai);
              //console.log("ojo aqui:"+vm.user.UrlPagina+" "+vm.user.UrlIntroduccion);
              setTimeout(function(){ vm.reloadRoute(); }, 2000);
           })
           .catch(function(err){
              console.log(err);
           });

          
        }
      }

      vm.reloadRoute = function() {

        console.log(vm.user);
         
        if(vm.user.Correo!="" && vm.user.Correo!=null && vm.user.Correo!=undefined
          && vm.user.Avatar!="" && vm.user.Avatar!=null && vm.user.Avatar!=undefined
          && vm.user.Identificacion!="" && vm.user.Identificacion!=null && vm.user.Identificacion!=undefined          
          && vm.user.Nombre!="" && vm.user.Nombre!=null && vm.user.Nombre!=undefined
          && vm.user.Direccion!="" && vm.user.Direccion!=null && vm.user.Direccion!=undefined
          && vm.user.Telefono!="" && vm.user.Telefono!=null && vm.user.Telefono!=undefined
          && vm.user.TelefonoWhatsApp!="" && vm.user.TelefonoWhatsApp!=null && vm.user.TelefonoWhatsApp!=undefined          
          //&& 
          )
          {
            if(vm.user.Proveedor==true){
              if( vm.user.NombreEmpresa!="" && vm.user.NombreEmpresa!=null && vm.user.NombreEmpresa!=undefined
                  && vm.user.Nit!="" && vm.user.Nit!=null && vm.user.Nit!=undefined 
                  && vm.user.UrlPagina!="" && vm.user.UrlPagina!=null && vm.user.UrlPagina!=undefined 
                  && vm.user.UrlIntroduccion!="" && vm.user.UrlIntroduccion!=null && vm.user.UrlIntroduccion!=undefined
                  && vm.user.Ubicacion!="" && vm.user.Ubicacion!=null && vm.user.Ubicacion!=undefined
                  && vm.user.Descripcion!=""  && vm.user.Descripcion!=null && vm.user.Descripcion!=undefined
                  ){
                $window.location = '#/index';
              }else{
                alert('Por favor completa tu registro');
                $window.location = '#/usuario/perfil';
              }
            }else{
              $window.location = '#/index';
            }
          }
          else{
            alert('Por favor completa tu registro');
            $window.location = '#/usuario/perfil';
          }
         
        
          $window.location.reload();
      }

      init();
      return vm;
    }]);