/**
 * easy-store Module
 *
 * Description
 */
angular
  .module('easy-store', [
    'angular-loading-bar',
    'dang-jssor',
    'ngDialog',
    'ngFileUpload',
    'ngResource',
    'ngRoute',
    'satellizer',
    'ngYoutubeEmbed',
    'multipleSelect'

  ])
  .constant('AppConst', {
    Url: 'http://api.prevenirexpress.com/api/v00',

    //UrlCitas: 'http://localhost/express-citas/site/servicio/',
    UrlCitas: 'http://citas.prevenirexpress.com/servicio',
    //Url: 'http://localhost:3000/api/v00',
    //FBAppId: '1042121939220395'
    FBAppId: '391411497886505'
  })
  .config([
    '$routeProvider',
    '$httpProvider',
    '$authProvider',
    'AppConst',
    '$sceDelegateProvider',
    function($routeProvider, $httpProvider, $authProvider, AppConst,$sceDelegateProvider) {
      // $httpProvider.interceptors.push('TokenInterceptor');

      $sceDelegateProvider.resourceUrlWhitelist([
          'self',
          'https://www.youtube.com/**'
        ]);

      // Parametros de configuración autenticación
      $authProvider.loginUrl    = [AppConst.Url, 'usuarios', 'login'].join('/');
      $authProvider.signupUrl   = [AppConst.Url, 'usuarios', 'signup'].join('/');
      $authProvider.tokenName   = 'Token';
      $authProvider.tokenPrefix = 'easy-store';
      $authProvider.facebook({
        clientId: AppConst.FBAppId,
        responseType: 'token',
        requiredUrlParams: ['display', 'scope'],
        redirectUri: window.location.origin + '/auth/facebook/facebook/callback',
        scope: ['email'],
        scopeDelimiter: ',',
        display: 'popup',
        oauthType: '2.0'
      });

      $routeProvider
        .when('/index', {
          templateUrl: 'easy-store/views/home/index.html',
          controller: 'HomeController',
          controllerAs: 'vm'
        })
        .when('/publicaciones/:titulo/:Id', {
          templateUrl: 'easy-store/views/publicacion/publicacion.html',
          controller: 'PublicacionController',
          controllerAs: 'vm'
        })
        .when('/servicios', {
          templateUrl: 'easy-store/views/servicios/index.html',
          controller: 'ServiciosController',
          controllerAs: 'vm'
        })
        .when('/publicar', {
          templateUrl: 'easy-store/views/publicar/index.html',
          controller: 'PublicarController',
          controllerAs: 'vm'
        })
        .when('/quienes-somos', {
          templateUrl: 'easy-store/views/quienes-somos/index.html',
          controller: 'QuienesSomosController',
          controllerAs: 'vm'
        })
        .when(
          '/resultados/:categoriaId/:IdMunicipio/page/:page', {
          templateUrl: 'easy-store/views/publicacion/list.html',
          controller: 'ListController',
          controllerAs: 'vm'
        })
        .when(
          '/resultados/:categoriaId/:subCategoriaId/:IdMunicipio/page/:page', {
          templateUrl: 'easy-store/views/publicacion/list.html',
          controller: 'ListController',
          controllerAs: 'vm'
        })
        .when(
          '/resultados/:categoriaId/:subCategoriaId/:variedadId/:IdMunicipio/page/:page', {
          templateUrl: 'easy-store/views/publicacion/list.html',
          controller: 'ListController',
          controllerAs: 'vm'
        })
        .when(
          '/resultados/:categoriaId/:subCategoriaId/:variedadId/:Descripcion/:IdMunicipio/page/:page', {
          templateUrl: 'easy-store/views/publicacion/list.html',
          controller: 'ListController',
          controllerAs: 'vm'
        })
        .when(
          '/resultados/:categoriaId/:subCategoriaId/:variedadId/:Descripcion/:IdMunicipio/page/:page', {
          templateUrl: 'easy-store/views/publicacion/list.html',
          controller: 'ListController',
          controllerAs: 'vm'
        })
        .when(
          '/usuario/publicaciones', {
          templateUrl: 'easy-store/views/publicacion/list-usuario.html',
          controller: 'UsuarioAdminController',
          controllerAs: 'vm'
        })
        .when(
          '/publicacionesusuario/:Id/:Nombre', {
          templateUrl: 'easy-store/views/publicacion/publicacionesusuario.html',
          controller: 'PublicacionesUsuarioController',
          controllerAs: 'vm'
        })
        .when(
          '/usuario/publicaciones/:Id/editar', {
          templateUrl: 'easy-store/views/publicacion/editar.html',
          controller: 'EditarPublicacionController',
          controllerAs: 'vm'
        })
        .when(
          '/usuario/publicaciones/nueva', {
          templateUrl: 'easy-store/views/publicacion/nueva.html',
          controller: 'EditarPublicacionController',
          controllerAs: 'vm'
        })
        .when(
          '/usuario/perfil', {
          templateUrl: 'easy-store/views/usuario/perfil.html',
          controller: 'PerfilUsuarioController',
          controllerAs: 'vm'
        })
        .when('/usuario/iniciar',{
          templateUrl: 'easy-store/views/home/iniciar.html',
          controller: 'SesionController',
          controllerAs: 'vm'

        })
        .when('/usuario/contrasena',{
          templateUrl: 'easy-store/views/home/cambiarcontrasena.html',
          controller: 'ContrasenaController',
          controllerAs: 'vm'
        })
        .when('/usuario/nuevacontra/:Id',{
          templateUrl: 'easy-store/views/home/nuevacontra.html',
          controller: 'ContrasenaController',
          controllerAs: 'vm'
        })
        .when('/usuario/registro',{
          templateUrl: "easy-store/views/home/registro.html",
          controller: "RegistroController",
          controllerAs: 'vm'
        })
        .when('/terminos-y-condiciones', {
            templateUrl: 'easy-store/views/quienes-somos/terminos-y-condiciones.html',
            controller: 'QuienesSomosController',
            controllerAs: 'vm'
        })
          .when('/plan1', {
            templateUrl: 'easy-store/views/planes/plan1.html',
            controller: 'PlanesController',
            controllerAs: 'vm'
        })
        .when('/plan2', {
          templateUrl: 'easy-store/views/planes/plan2.html',
          controller: 'PlanesController',
          controllerAs: 'vm'
      })
      .when('/plan3', {
        templateUrl: 'easy-store/views/planes/plan3.html',
        controller: 'PlanesController',
        controllerAs: 'vm'
    })
      .when('/plan4', {
        templateUrl: 'easy-store/views/planes/plan4.html',
        controller: 'PlanesController',
        controllerAs: 'vm'
    })
        .when('/planes', {
          templateUrl: 'easy-store/views/planes/planes.html',
          controller: 'PlanesController',
          controllerAs: 'vm'
    })
      .when('/', {
        templateUrl: 'easy-store/views/home/principal.html',
        controller: 'PrincipalController',
        controllerAs: 'vm'
      })
      .when('/usuario/registroFamilia',{
        templateUrl: "easy-store/views/home/registroFamilia.html",
        controller: "RegistroFamiliaController",
        controllerAs: 'vm'
      })
      .when('/contactanos',{
        templateUrl: "easy-store/views/home/contactoNuevo.html",
        controller: "ContactoController",
        controllerAs: 'vm'
      })
      .when('/aprobacion/:titulo/:Id',{
        templateUrl: "easy-store/views/publicacion/aprobacion.html",
        controller: "AprobacionController",
        controllerAs: 'vm'
      })
       .when('/empresas', {
          templateUrl: 'easy-store/views/planes/empresas.html',
          controller: 'PlanesController',
          controllerAs: 'vm'
    });

    }])
  .run([
    '$rootScope',
    '$auth',
    function($rootScope, $auth) {
      $rootScope.$on('$routeChangeSuccess', function(angularEvent, current, previous) {
        //console.log(angularEvent);
        //console.log(current);
        //console.log(previous);
      });
    }]);



lightbox('.lightbox',{
captions: true,
captionsSelector: "self",
captionAttribute: "title",
nav: "auto",
navText: ["‹", "›"],
close: true,
closeText: "×",
counter: true,
keyboard: true,
zoom: true,
zoomText: "+",
docClose: false,
swipeClose: true,
scroll: false
});
