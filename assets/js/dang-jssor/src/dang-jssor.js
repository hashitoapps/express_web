angular.module("dang-jssor", [])
	.directive("enableJssor", function () {
	    return {
	        restrict: "A",
	        scope: {
	            jssorOptions: "="
	        },
	        link: function (scope, element, attrs) {
	            if (attrs.jssorTrigger == 'true') {
	                var container = $(element).closest('.slides-container');
	                
	                if (!container.attr("id"))
	                	container.attr("id", new Date().getTime());

	                var slider = new $JssorSlider$(container.attr("id"), scope.jssorOptions);
                
	                slider.$On($JssorSlider$.$EVT_PARK, function (slideIndex, fromIndex) {
	                    var status = null;
	                    scope.$emit("JssorSliderChanged", status = {
	                        name: scope.jssorOptions.name,
	                        slideIndex: slideIndex,
	                        fromIndex: fromIndex
	                    });

	                    if (scope.jssorOptions.name) {
	                        console.log("SliderChanged:", scope.jssorOptions.name, angular.toJson(status));
	                    }
	                    scope.$apply();
	                });

	                if (scope.jssorOptions.$fullWidth === true) {
						function ScaleSlider() {
						    var parentWidth = $('#slider1_container').parent().width();
						    if (parentWidth) {
						        slider.$ScaleWidth(parentWidth);
						    }
						    else
						        window.setTimeout(ScaleSlider, 30);
						}
						//Scale slider after document ready
						ScaleSlider();
						                                
						//Scale slider while window load/resize/orientationchange.
						$(window).bind("load", ScaleSlider);
						$(window).bind("resize", ScaleSlider);
						$(window).bind("orientationchange", ScaleSlider);
						//responsive code end	                	
	                };
	            }
	        }
	    }
	});